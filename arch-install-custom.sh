#!/usr/bin/expect
set timeout 360
spawn archinstall
expect "Select one of the obove keyboard languages (by number or full name):" { send "26" }
expect "Select one of the obove regions to download packages from (by number or full name):" { send "41" }
expect "Select one or more harddrives to use and configure (leave blank to skip this step):" { send "0" send "\r" }
expect "Select what you wish to do with the selected block devices:" { send "0" }
expect "Select which filesystem your main partition should use (by number or name):" { send "0" }
expect "Would you like to use BTRFS subvolumes with a default structure? (Y/n):" { send "y" }
expect "Enter disk encryption password" { send "\r" }
expect "Would you like to use swap on zram? (Y/n)" { send "y" }
expect "Desired hostname for the installation:" { send "WSNL01" }
expect "Enter root password (leave blank to disable root & create superuser):" { send "\r" }
expect "Create a required super-user with sudo privileges:" { send "nlohuis" }
expect "Password for user nlohuis:" { send "admin" }
expect "And one more time for verification:" { send "admin" }
expect "Enter a username to create an additional user (leave blank to skip & continue):" { send "\r" }
expect "Enter a pre-programmed profile name if you want to install one:" { send "0" }
expect "Select your desired desktop environment:" { send "5" }
expect "Select a graphics driver or leave blank to install all open-source drivers:" { send "\r" }
expect "Choose an audio server or leave blank to use pipewire:" { send "\r" }
expect "Choose which kernels to use (leave blank for default: linux):" { send "\r" }
expect "Write additional packages to install (space separated, leave blank to skip):" { send "git" }
expect "Select one network interface to configure (leave blank to skip):" { send "1" }
expect "Enter a valid timezone (examples: Europe/Stockholm, US/Eastern) or press enter to use UTC:" { send "Europe/Amsterdam" }
expect "Would you like to use automatic time synchronization (NTP) with the default time servers? [Y/n]:" { send "y" }
expect "Press Enter to continue." { send "\r" }