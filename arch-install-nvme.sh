pacman -S git

git clone https://gitlab.com/nicklohuis/archinstall.git

cd /archinstall

python -m archinstall --config arch-config.json --disk_layouts arch-config-disks-nvme.json --creds arch-config-creds.json

reboot